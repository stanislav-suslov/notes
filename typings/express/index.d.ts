declare namespace Express {
  export interface Request {
    userId?: number
  }

  export interface Response {
    sendValidationErrors(errors: import('class-validator').ValidationError[]): this
    setTokens(tokens?: import('~/database/entities/user/types').TokenData): this
  }
}
