import { NextFunction, Request, Response } from 'express'
import { UserService } from '~/services/user'

export const getAuthenticated = (userService: UserService) => {
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { access_token, refresh_token } = req.cookies

    const { userId, tokens } = await userService.authenticate(access_token, refresh_token)
    if (tokens) {
      res.setTokens(tokens)
    }

    req.userId = userId

    return next()
  }
}

/**
 * При отсутствии пользователя не выбрасывает ошибку
 */
export const getSafeAuthenticated = (userService: UserService) => {
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { access_token, refresh_token } = req.cookies

    const result = await userService.checkAuthentication(access_token, refresh_token)

    if (result) {
      if (result.tokens) {
        res.setTokens(result.tokens)
      }

      req.userId = result.userId
    }

    return next()
  }
}
