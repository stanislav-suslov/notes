export abstract class HttpError extends Error {
  abstract httpStatus: number
  abstract message: string
}
