import * as express from 'express'
import { Server } from 'http'
import { PORT } from '~/environment'
import { UserService } from '~/services/user'
import { getUserRoutes } from './routes/user'
import 'express-async-errors'
import { json } from 'body-parser'
import * as cookieParser from 'cookie-parser'
import { HttpError } from './errors'
import { getNoteRoutes } from './routes/note'
import { NoteService } from '~/services/note'
import { extendResponse } from './utils'

export function createServer(): Server {
  const app = express()

  app.use(express.urlencoded({ extended: true }))
  app.use(json())
  app.use(cookieParser())

  extendResponse(app.response)

  const userService = new UserService()
  const noteService = new NoteService()

  app.use('/user', getUserRoutes(userService))
  app.use('/notes', getNoteRoutes(noteService, userService))

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (err instanceof HttpError) {
      return res.status(err.httpStatus).json({ error: err.message })
    }

    console.error(err)
    res.status(500).send()
  })

  return app.listen(PORT, () => {
    console.log(`Server started at port ${PORT}`)
  })
}
