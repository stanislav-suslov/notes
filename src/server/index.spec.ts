/* eslint-disable jest/no-test-return-statement */
import * as supertest from 'supertest'
import { getConnection } from '~/database'
import { createServer } from '.'

describe('e2e', () => {
  const server = createServer()
  const login = 'test login one'
  const password = 'test password one'
  let cookiesAfterRegistration = ''
  let cookies = ''
  let noteId = ''

  // eslint-disable-next-line jest/no-hooks
  afterAll(() => {
    getConnection().then((c) => c.close())
    server.close()
  })

  describe('пользователь', () => {
    it('1. регистрируется и получает куки', () => {
      expect.assertions(2)

      return supertest(server)
        .post('/user/register')
        .send({ login, password })
        .then((response) => {
          expect(response.status).toBe(201)
          expect(response.headers).toHaveProperty('set-cookie')
          cookies = response.headers['set-cookie']
          cookiesAfterRegistration = cookies
        })
    })

    it('2. разлогинивается и получает куки', () => {
      expect.assertions(2)

      return supertest(server)
        .get('/user/logout')
        .set('Cookie', [cookies])
        .then((response) => {
          expect(response.status).toBe(200)
          expect(response.headers).toHaveProperty('set-cookie')
          cookies = response.headers['set-cookie']
        })
    })

    it('3. логинится и получает отличающиеся от регистрационных куки', () => {
      expect.assertions(3)

      return supertest(server)
        .post('/user/login')
        .send({ login, password })
        .then((response) => {
          expect(response.status).toBe(200)
          expect(response.headers).toHaveProperty('set-cookie')
          cookies = response.headers['set-cookie']
          expect(cookies).not.toBe(cookiesAfterRegistration)
        })
    })

    it('4. добавляет заметку', () => {
      expect.assertions(1)

      return supertest(server)
        .post('/notes')
        .set('Cookie', [cookies])
        .send({ content: 'test note 1' })
        .then((response) => {
          expect(response.status).toBe(201)
          noteId = response.body.note.id
        })
    })

    it('5. получает заметку', () => {
      expect.assertions(2)

      return supertest(server)
        .get(`/notes/${noteId}`)
        .set('Cookie', [cookies])
        .then((response) => {
          expect(response.status).toBe(200)
          expect(response.body.note.id).toBe(noteId)
        })
    })

    it('5.1. запрещает неавторизованному пользователю доступ к заметке', () => {
      expect.assertions(1)

      return supertest(server)
        .get(`/notes/${noteId}`)
        .then((response) => {
          expect(response.status).toBe(404)
        })
    })

    it('6. обновляет текст и публичность заметки', () => {
      expect.assertions(1)

      return supertest(server)
        .put(`/notes/${noteId}`)
        .set('Cookie', [cookies])
        .send({ content: 'test note 1 updated', public: true })
        .then((response) => {
          expect(response.status).toBe(200)
        })
    })

    it('6.1. позволяет неавторизованному пользователю просмотреть запись', () => {
      expect.assertions(2)

      return supertest(server)
        .get(`/notes/${noteId}`)
        .then((response) => {
          expect(response.status).toBe(200)
          expect(response.body.note.id).toBe(noteId)
        })
    })

    it('7. получает список заметок', () => {
      expect.assertions(2)

      return supertest(server)
        .get('/notes')
        .set('Cookie', [cookies])
        .then((response) => {
          expect(response.status).toBe(200)
          const [note] = response.body.notes
          expect(note.content).toBe('test note 1 updated')
        })
    })
  })
})
