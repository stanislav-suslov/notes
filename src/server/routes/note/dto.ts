import { Type } from 'class-transformer'
import { IsBoolean, IsInt, IsOptional, IsPositive, IsString, Length } from 'class-validator'

export class GetNotesDto {
  @IsInt()
  @IsPositive()
  @Type(() => Number)
  offset!: number

  @IsInt()
  @IsPositive()
  @Type(() => Number)
  limit!: number
}

export class CreateNoteDto {
  @IsString()
  @Length(1, 1000)
  content!: string

  @IsOptional()
  @IsBoolean()
  public?: boolean
}

export class UpdateNoteDto {
  @IsOptional()
  @IsString()
  @Length(1, 1000)
  content!: string

  @IsOptional()
  @IsBoolean()
  public?: boolean
}
