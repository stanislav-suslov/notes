import { Router } from 'express'
import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { NoteService } from '~/services/note'
import { getAuthenticated, getSafeAuthenticated } from '~/server/middlewares/authenticated'
import { UserService } from '~/services/user'
import { CreateNoteDto, GetNotesDto, UpdateNoteDto } from './dto'

export function getNoteRoutes(noteService: NoteService, userService: UserService): Router {
  const router = Router()
  const auth = getAuthenticated(userService)
  const authSafe = getSafeAuthenticated(userService)

  router.get('/', auth, async (req, res) => {
    const query = plainToClass(GetNotesDto, req.query)
    const errors = await validate(query, { skipMissingProperties: true })
    if (errors.length) {
      return res.sendValidationErrors(errors)
    }

    const { params, notes } = await noteService.getNotes(req.userId!, query.offset, query.limit)

    return res.json({
      params,
      notes,
    })
  })

  router.get('/:note', authSafe, async (req, res) => {
    const note = await noteService.getNote(req.params.note, req.userId)

    return res.status(200).json({ note })
  })

  router.post('/', auth, async (req, res) => {
    const body = plainToClass(CreateNoteDto, req.body)
    const errors = await validate(body)
    if (errors.length) {
      return res.sendValidationErrors(errors)
    }

    const note = await noteService.createNote(req.userId!, body.content, body.public)

    return res.status(201).json({ note })
  })

  router.put('/:note', auth, async (req, res) => {
    const body = plainToClass(UpdateNoteDto, req.body)
    const errors = await validate(body)
    if (errors.length) {
      return res.sendValidationErrors(errors)
    }

    const note = await noteService.updateNote(
      req.userId!,
      req.params.note,
      body.content,
      body.public,
    )

    return res.status(200).json({ note })
  })

  router.delete('/:note', auth, async (req, res) => {
    await noteService.deleteNote(req.userId!, req.params.note)

    return res.status(200).json()
  })

  return router
}
