import { IsString } from 'class-validator'

export class UserAuthDto {
  @IsString()
  public login!: string
  @IsString()
  public password!: string
}
