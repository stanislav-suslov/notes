import { Router } from 'express'
import { UserService } from '~/services/user'
import { plainToClass } from 'class-transformer'
import { UserAuthDto } from './dto'
import { validate } from 'class-validator'

export function getUserRoutes(userService: UserService): Router {
  const router = Router()

  router.post('/register', async (req, res) => {
    const body = plainToClass(UserAuthDto, req.body)
    const errors = await validate(body)
    if (errors.length) {
      return res.sendValidationErrors(errors)
    }

    const tokens = await userService.register(req.body.login, req.body.password)

    return res.status(201).setTokens(tokens).end()
  })

  router.post('/login', async (req, res) => {
    const body = plainToClass(UserAuthDto, req.body)
    const errors = await validate(body)
    if (errors.length) {
      return res.sendValidationErrors(errors)
    }

    const tokens = await userService.login(body.login, body.password)

    return res.setTokens(tokens).end()
  })

  router.get('/logout', async (req, res) => {
    const accessToken = req.cookies.access_token
    await userService.logout(accessToken)

    return res.setTokens().end()
  })

  return router
}
