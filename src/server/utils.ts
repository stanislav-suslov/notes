import { ValidationError } from 'class-validator'
import { Response } from 'express'
import { TokenData } from '~/database/entities/user/types'

export function extendResponse(response: Response): void {
  response.sendValidationErrors = function (errors: ValidationError[]) {
    return this.status(400).json({
      errors: errors.reduce<Record<string, string[]>>((acc, error) => {
        acc[error.property] = Object.values(error.constraints || {})
        return acc
      }, {}),
    })
  }

  response.setTokens = function (tokens?: TokenData) {
    if (tokens) {
      this.cookie('access_token', tokens.access, {
        httpOnly: true,
        expires: tokens.accessExpiresIn,
      })
      this.cookie('refresh_token', tokens.refresh, {
        httpOnly: true,
        expires: tokens.refreshExpiresIn,
      })
    } else {
      this.clearCookie('access_token')
      this.clearCookie('refresh_token')
    }

    return this
  }
}
