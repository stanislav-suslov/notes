export const PORT: number = parseInt(process.env.PORT || '80', 10)

export const DB = {
  HOST: process.env.DB_HOST || '',
  PORT: parseInt(process.env.DB_PORT || '5432', 10),
  USERNAME: process.env.DB_USERNAME || '',
  PASSWORD: process.env.DB_PASSWORD || '',
  DATABASE: process.env.DB_DATABASE || '',
}
