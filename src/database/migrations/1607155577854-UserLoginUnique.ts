import { MigrationInterface, QueryRunner } from 'typeorm'

export class UserLoginUnique1607155577854 implements MigrationInterface {
  name = 'UserLoginUnique1607155577854'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`COMMENT ON COLUMN "user"."login" IS NULL`)
    await queryRunner.query(
      `ALTER TABLE "user" ADD CONSTRAINT "UQ_a62473490b3e4578fd683235c5e" UNIQUE ("login")`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "UQ_a62473490b3e4578fd683235c5e"`)
    await queryRunner.query(`COMMENT ON COLUMN "user"."login" IS NULL`)
  }
}
