import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { User } from '../user'

@Entity()
export class Note {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string

  @Column({ type: 'varchar', length: 1000 })
  content!: string

  @ManyToOne(() => User, (user) => user.notes)
  @JoinColumn({ name: 'user_id' })
  user!: User

  @Column({ type: 'boolean', default: false })
  public!: boolean

  @CreateDateColumn({ name: 'created_at' })
  createdAt!: Date

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt!: Date
}
