export interface TokenData {
  access: string
  accessExpiresIn: Date
  refresh: string
  refreshExpiresIn: Date
}
