import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Note } from '../note'
import { TokenData } from './types'
import { v4 as uuid } from 'uuid'

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  readonly id!: number

  @Column({ type: 'varchar', length: 100, unique: true })
  login!: string

  @Column({ type: 'varchar', length: 100 })
  password!: string

  @Column({ name: 'access_token', length: 36, type: 'varchar', nullable: true })
  accessToken!: string | null

  @Column({ name: 'access_token_expires_in', type: 'timestamp', nullable: true })
  accessTokenExpiresIn!: Date | null

  @Column({ name: 'refresh_token', length: 36, type: 'varchar', nullable: true })
  refreshToken!: string | null

  @Column({ name: 'refresh_token_expires_in', type: 'timestamp', nullable: true })
  refreshTokenExpiresIn!: Date | null

  @OneToMany(() => Note, (note) => note.user)
  notes!: Note[]

  setTokens(): TokenData {
    const data: TokenData = {
      access: uuid(),
      accessExpiresIn: new Date(+new Date() + 1000 * 86400 * 14),
      refresh: uuid(),
      refreshExpiresIn: new Date(+new Date() + 1000 * 86400 * 28),
    }

    this.accessToken = data.access
    this.accessTokenExpiresIn = data.accessExpiresIn
    this.refreshToken = data.refresh
    this.refreshTokenExpiresIn = data.refreshExpiresIn

    return data
  }

  removeTokens(): void {
    this.accessToken = null
    this.accessTokenExpiresIn = null
    this.refreshToken = null
    this.refreshTokenExpiresIn = null
  }
}
