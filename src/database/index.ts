import { Connection, createConnection, EntityTarget, Repository } from 'typeorm'

const promisedConnection = createConnection()

export function getConnection(): Promise<Connection> {
  return promisedConnection
}

export function getRepository<T = unknown>(target: EntityTarget<T>): Promise<Repository<T>> {
  return getConnection().then((connection) => connection.getRepository(target))
}
