export interface Note {
  id: string
  content: string
  public: boolean
  createdAt: Date
  updatedAt: Date
}

export interface NotesData {
  params: {
    count: number
    offset: number
    limit: number
  }
  notes: Note[]
}
