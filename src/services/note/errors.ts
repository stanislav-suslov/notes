import { HttpError } from '~/server/errors'

export class NoteNotExistsError extends HttpError {
  httpStatus = 404
  message = 'Note does not exists'

  constructor() {
    super()

    Object.setPrototypeOf(this, NoteNotExistsError.prototype)
  }
}
