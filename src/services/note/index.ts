import { getRepository } from '~/database'
import { Note } from '~/database/entities/note'
import { User } from '~/database/entities/user'
import { NoteNotExistsError } from './errors'
import { NotesData, Note as INote } from './types'

export class NoteService {
  constructor(
    private promisedNoteRepository = getRepository(Note),
    private promisedUserRepository = getRepository(User),
  ) {}

  private static processNote(note: Note): INote {
    return {
      id: note.id,
      content: note.content,
      public: note.public,
      createdAt: note.createdAt,
      updatedAt: note.updatedAt,
    }
  }

  async getNotes(
    userId: number,
    offset: number | undefined = 0,
    limit: number | undefined = 10,
  ): Promise<NotesData> {
    const noteRepository = await this.promisedNoteRepository

    const findLimit = Math.min(limit, 50)

    const [notes, count] = await noteRepository.findAndCount({
      where: { user: userId },
      take: findLimit,
      skip: offset,
    })

    return {
      params: {
        count,
        limit: findLimit,
        offset: offset,
      },
      notes: notes.map(NoteService.processNote),
    }
  }

  async getNote(noteId: string, userId?: number): Promise<INote> {
    const noteRepository = await this.promisedNoteRepository

    const note = await noteRepository.findOne({
      where: { id: noteId },
      relations: ['user'],
    })

    if (!note) {
      throw new NoteNotExistsError()
    }

    if (!note.public && userId !== note.user.id) {
      throw new NoteNotExistsError()
    }

    return NoteService.processNote(note)
  }

  async createNote(userId: number, content: string, isPublic?: boolean): Promise<INote> {
    const noteRepository = await this.promisedNoteRepository
    const userRepository = await this.promisedUserRepository

    const note = new Note()
    note.content = content
    note.user = (await userRepository.findOne(userId))!

    if (isPublic !== undefined) {
      note.public = isPublic
    }

    return NoteService.processNote(await noteRepository.save(note))
  }

  async updateNote(
    userId: number,
    noteId: string,
    content?: string,
    isPublic?: boolean,
  ): Promise<INote> {
    const noteRepository = await this.promisedNoteRepository

    const note = await noteRepository.findOne({ where: { user: userId, id: noteId } })

    if (!note) {
      throw new NoteNotExistsError()
    }

    if (content) {
      note.content = content
    }

    if (isPublic !== undefined) {
      note.public = isPublic
    }

    return NoteService.processNote(await noteRepository.save(note))
  }

  async deleteNote(userId: number, noteId: string): Promise<void> {
    const noteRepository = await this.promisedNoteRepository

    const note = await noteRepository.findOne({ where: { id: noteId, user: userId } })

    if (!note) {
      throw new NoteNotExistsError()
    }

    await noteRepository.delete({ id: noteId })
  }
}
