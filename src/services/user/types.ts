import { TokenData } from '~/database/entities/user/types'

export interface CheckAuthResult {
  userId: number
  tokens?: TokenData
}
