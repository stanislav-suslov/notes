import { HttpError } from '~/server/errors'

export class UserNotFoundError extends HttpError {
  httpStatus = 400
  message = 'User not found'

  constructor() {
    super()

    Object.setPrototypeOf(this, UserNotFoundError.prototype)
  }
}

export class UserAlreadyExistsError extends HttpError {
  httpStatus = 400
  message = 'User already exists'

  constructor() {
    super()

    Object.setPrototypeOf(this, UserAlreadyExistsError.prototype)
  }
}
