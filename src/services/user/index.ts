import { Raw } from 'typeorm'
import { getRepository } from '~/database'
import { User } from '~/database/entities/user'
import { TokenData } from '~/database/entities/user/types'
import { UserAlreadyExistsError, UserNotFoundError } from './errors'
import { CheckAuthResult } from './types'

export class UserService {
  constructor(private promisedUserRepository = getRepository(User)) {}

  private async getUserByToken(
    type: 'access' | 'refresh',
    token: string,
  ): Promise<User | undefined> {
    return (await this.promisedUserRepository).findOne({
      where: {
        [`${type}Token`]: token,
        [`${type}TokenExpiresIn`]: Raw((alias) => `${alias} > NOW()`),
      },
    })
  }

  public async checkAuthentication(
    accessToken: string,
    refreshToken: string,
  ): Promise<CheckAuthResult | null> {
    const userAccess = await this.getUserByToken('access', accessToken)
    if (userAccess) {
      return { userId: userAccess.id }
    }

    const user = await this.getUserByToken('access', refreshToken)

    if (user) {
      const tokens = user.setTokens()
      await (await this.promisedUserRepository).save(user)
      return { userId: user.id, tokens }
    }

    return null
  }

  public async authenticate(accessToken: string, refreshToken: string): Promise<CheckAuthResult> {
    const result = await this.checkAuthentication(accessToken, refreshToken)

    if (!result) {
      throw new UserNotFoundError()
    }

    return result
  }

  public async register(login: string, password: string): Promise<TokenData> {
    const userRepository = await this.promisedUserRepository
    if (await userRepository.findOne({ where: { login } })) {
      throw new UserAlreadyExistsError()
    }

    const user = new User()
    user.login = login
    user.password = password
    const tokens = user.setTokens()
    await userRepository.save(user)
    return tokens
  }

  public async login(login: string, password: string): Promise<TokenData> {
    const userRepository = await this.promisedUserRepository

    const user = await userRepository.findOne({ where: { login, password } })

    if (!user) {
      throw new UserNotFoundError()
    }

    const tokens = user.setTokens()
    await userRepository.save(user)
    return tokens
  }

  public async logout(accessToken: string): Promise<void> {
    const userRepository = await this.promisedUserRepository
    const user = await userRepository.findOne({ where: { accessToken } })

    if (!user) {
      throw new UserNotFoundError()
    }

    user.removeTokens()
    await userRepository.save(user)
  }
}
