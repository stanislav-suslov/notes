import { ConnectionOptions } from 'typeorm'
import { User } from './src/database/entities/user'
import { Note } from './src/database/entities/note'
import { DB } from './src/environment'

const options: ConnectionOptions = {
  type: 'postgres',
  host: DB.HOST,
  port: DB.PORT,
  username: DB.USERNAME,
  password: DB.PASSWORD,
  database: DB.DATABASE,
  entities: [User, Note],
  migrations: ['src/database/migrations/**/*.ts'],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
}

export default options
